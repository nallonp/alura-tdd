package br.com.alura.tdd;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalculadoraTest {

  Calculadora calculadora;

  @BeforeEach
  void setup() {
    this.calculadora = new Calculadora();
  }

  @Test
  void deveriaSomarDoisNumerosPositivos() {
    int soma = this.calculadora.somar(1, 2);
    assertEquals(3, soma);
  }
}