package br.com.alura.tdd.modelo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Funcionario {

  private String nome;
  private LocalDate dataAdmissao;
  private BigDecimal salario;

  public void reajustarSalario(BigDecimal reajuste) {
    this.salario = this.salario.add(reajuste);
    arredondarSalario();
  }

  // Método privado não precisa ser testado separadamente.
  private void arredondarSalario() {
    this.salario = this.salario.setScale(2, RoundingMode.HALF_UP);
  }
}
